/*
 * Copyright (c) 2009, Sun Microsystems, Inc. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *  . Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  . Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  . Neither the name of Sun Microsystems, Inc., nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.jdesktop.mtgame.util;

import com.jme3.bounding.BoundingBox;
import com.jme3.material.RenderState;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.mesh.IndexBuffer;
import com.jme3.util.BufferUtils;
import org.jdesktop.mtgame.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * This class contains geometry utilities
 * 
 * @author Doug Twilleager
 */
public class Geometry {
    private static Object utilLock = new Object();

    /**
     * The constructor
     */
    public Geometry() {
    }

    public static Node explodeIntoSpatials(WorldManager wm, Mesh mesh) {
        synchronized (utilLock) {
            Mesh srcMesh = mesh;
            Mesh sharedMesh = null;

            FloatBuffer normals = srcMesh.getFloatBuffer(VertexBuffer.Type.Normal);
            FloatBuffer verts = srcMesh.getFloatBuffer(VertexBuffer.Type.Position);
            FloatBuffer colors = srcMesh.getFloatBuffer(VertexBuffer.Type.Color);
            FloatBuffer tangents = srcMesh.getFloatBuffer(VertexBuffer.Type.Tangent);
            FloatBuffer binormals = srcMesh.getFloatBuffer(VertexBuffer.Type.Binormal);
            IndexBuffer indicies = srcMesh.getIndexBuffer();
            FloatBuffer texcs = srcMesh.getFloatBuffer(VertexBuffer.Type.TexCoord);

            int j = 0;
            Node node = new Node();
            for (int i=0; i<mesh.getTriangleCount(); i++) {
                int i1 = indicies.get(j++);
                int i2 = indicies.get(j++);
                int i3 = indicies.get(j++);
                node.attachChild(createTriMesh(wm, srcMesh, i1, i2, i3, verts, normals, colors,
                                               texcs, tangents, binormals, sharedMesh));
            }
            return (node);
        }

    }

    private static Spatial createTriMesh(WorldManager wm, Mesh srcMesh, int oi1, int oi2, int oi3,
                                         FloatBuffer verts, FloatBuffer normals, FloatBuffer colors,
                                         FloatBuffer texcs, FloatBuffer tangents, FloatBuffer binormals,
                                         Mesh sharedMesh) {
        Mesh newMesh = new Mesh( );
        FloatBuffer v = null;
        FloatBuffer n = null;
        FloatBuffer c = null;
//        TexCoords tc = null;
        FloatBuffer tan = null;
        FloatBuffer bin = null;

        if (verts != null) {
            float[] data = new float[3*3];
            int i1 = oi1*3;
            int i2 = oi2*3;
            int i3 = oi3*3;
            data[0] = verts.get(i1); data[1] = verts.get(i1+1); data[2] = verts.get(i1+2);
            data[3] = verts.get(i2); data[4] = verts.get(i2+1); data[5] = verts.get(i2+2);
            data[6] = verts.get(i3); data[7] = verts.get(i3+1); data[8] = verts.get(i3+2);
            v = BufferUtils.createFloatBuffer(data);
            
            newMesh.setBuffer(VertexBuffer.Type.Position, data.length, v);
        }

        if (normals != null) {
            float[] data = new float[3*3];
            int i1 = oi1*3;
            int i2 = oi2*3;
            int i3 = oi3*3;
            data[0] = normals.get(i1); data[1] = normals.get(i1+1); data[2] = normals.get(i1+2);
            data[3] = normals.get(i2); data[4] = normals.get(i2+1); data[5] = normals.get(i2+2);
            data[6] = normals.get(i3); data[7] = normals.get(i3+1); data[8] = normals.get(i3+2);
            n = BufferUtils.createFloatBuffer(data);
            
            newMesh.setBuffer(VertexBuffer.Type.Normal, data.length, n);
        }

        if (colors != null) {
            float[] data = new float[4*3];
            int i1 = oi1*3;
            int i2 = oi2*3;
            int i3 = oi3*3;
            data[0] = colors.get(i1); data[1] = colors.get(i1+1); data[2] = colors.get(i1+2); data[3] = colors.get(i1+3);
            data[4] = colors.get(i2); data[5] = colors.get(i2+1); data[6] = colors.get(i2+2); data[7] = colors.get(i2+3);
            data[8] = colors.get(i3); data[9] = colors.get(i3+1); data[10] = colors.get(i3+2); data[11] = colors.get(i3+3);
            c = BufferUtils.createFloatBuffer(data);
            
            
            newMesh.setBuffer(VertexBuffer.Type.Color, data.length, c);
        }

//        if (texcs. != 0) {
//            // Just one for now
//            TexCoords tcs = (TexCoords)texcs.get(0);
//            float[] data = new float[tcs.perVert*3];
//            int i1 = oi1*tcs.perVert;
//            int i2 = oi2*tcs.perVert;
//            int i3 = oi3*tcs.perVert;
//            int index = 0;
//            for (int i=0; i<tcs.perVert; i++) {
//                data[index++] = tcs.coords.get(i1+i);
//            }
//            for (int i=0; i<tcs.perVert; i++) {
//                data[index++] = tcs.coords.get(i2+i);
//            }
//            for (int i=0; i<tcs.perVert; i++) {
//                data[index++] = tcs.coords.get(i3+i);
//            }
//            FloatBuffer tcb = BufferUtils.createFloatBuffer(data);
//            tc = new TexCoords(tcb, tcs.perVert);
//        }

        int[] ind = new int[3];
        ind[0] = 0; ind[1] = 1; ind[2] = 2;
        IntBuffer ib = BufferUtils.createIntBuffer(ind);

        newMesh.setBuffer(VertexBuffer.Type.Index, ind.length, ib);

         if (tangents != null) {
            float[] data = new float[3*3];
            int i1 = oi1*3;
            int i2 = oi2*3;
            int i3 = oi3*3;
            data[0] = tangents.get(i1); data[1] = tangents.get(i1+1); data[2] = tangents.get(i1+2);
            data[3] = tangents.get(i2); data[4] = tangents.get(i2+1); data[5] = tangents.get(i2+2);
            data[6] = tangents.get(i3); data[7] = tangents.get(i3+1); data[8] = tangents.get(i3+2);
            tan = BufferUtils.createFloatBuffer(data);
            
            newMesh.setBuffer(VertexBuffer.Type.Tangent, data.length, tan);
        }

        if (binormals != null) {
            float[] data = new float[3*3];
            int i1 = oi1*3;
            int i2 = oi2*3;
            int i3 = oi3*3;
            data[0] = binormals.get(i1); data[1] = binormals.get(i1+1); data[2] = binormals.get(i1+2);
            data[3] = binormals.get(i2); data[4] = binormals.get(i2+1); data[5] = binormals.get(i2+2);
            data[6] = binormals.get(i3); data[7] = binormals.get(i3+1); data[8] = binormals.get(i3+2);
            bin = BufferUtils.createFloatBuffer(data);
            
            newMesh.setBuffer(VertexBuffer.Type.Binormal, data.length, bin);
        }

        // Copy render states
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Blend));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Clip));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.ColorMask));
//        CullState cs = (CullState)sharedMesh.getRenderState(RenderState.StateType.Cull);
//        if (cs == null) {
//            System.out.println("======================== CS");
//            cs = (CullState)wm.getRenderManager().createRendererState(RenderState.StateType.Cull);
//        }
//        cs.setCullFace(CullState.Face.None);
//        cs.setEnabled(true);
//        newMesh.setRenderState(cs);
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Cull));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Fog));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.FragmentProgram));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.GLSLShaderObjects));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Light));
//        MaterialState ms = (MaterialState)sharedMesh.getRenderState(RenderState.StateType.Material);
//        if (ms == null) {
//            System.out.println("======================== MS");
//            ms = (MaterialState)wm.getRenderManager().createRendererState(RenderState.StateType.Material);
//        }
//        ms.setMaterialFace(MaterialState.MaterialFace.FrontAndBack);
//        ms.setEnabled(true);
//        newMesh.setRenderState(ms);
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Material));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Shade));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Stencil));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Texture));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.VertexProgram));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.Wireframe));
//        newMesh.setRenderState(sharedMesh.getRenderState(RenderState.StateType.ZBuffer));
//
//        newMesh.setLocalScale(sharedMesh.getLocalScale());
//        newMesh.setLocalRotation(sharedMesh.getLocalRotation());
//        newMesh.setLocalTranslation(sharedMesh.getLocalTranslation());

        // Finally, the bounds
        BoundingBox bbox = new BoundingBox();
        bbox.computeFromTris(ind, newMesh, 0, 2);
        newMesh.setBound(bbox);
        return new com.jme3.scene.Geometry("", newMesh);
    }
}
